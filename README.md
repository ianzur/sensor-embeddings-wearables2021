# sensor embedding -- AI wearables fall 2021 
See `git_usage_guide.md` for help setting up your local copy of this repo


- dataset split design
    - see definition: `utils/cross_validation_splits.py`
- encoding model design: `autoencoder/models.py`
    - TODO: data preprocessing (mean, std) and/or gravity filtering
    - dense model
        - simple dense
    - activity2vec, https://arxiv.org/pdf/1907.05597.pdf
        - RNN encoder 
- classification model design
    - kiss
- which (if any) time stamp to use?do we want to do this? 
    - we ignore time.
- expert features (human defined features)
    - TODO: someone could compare this performs (speed / classification accuracy)
- data normalization
    - compensate for gravity with a [high pass filter](https://developer.android.com/reference/android/hardware/SensorEvent#sensor.type_accelerometer:)?
        - requires a timestep

**tensorflow-dataset**
`pip install tensorflow-dataset`

See: `smartwatch_gestures/smartwatch_gestures.py`
A config file for the smartwatch-gestures. 
[tfds](https://www.tensorflow.org/datasets/add_dataset) has lots of functionality to set-up a dataset for tensorflow training. see: 



