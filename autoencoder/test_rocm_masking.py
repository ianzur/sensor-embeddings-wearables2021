"""
masking is not currently support for RNN layers with ROCm (AMD gpus)
"""

import numpy as np
import tensorflow as tf


def fake_ragged_data():
    """create fake ragged data, return concatenated data and row splits"""

    examples = []
    examples.append(np.random.rand(4, 3))
    examples.append(np.random.rand(3, 3))
    examples.append(np.random.rand(5, 3))
    examples.append(np.random.rand(2, 3))

    row_splits = [0]
    for i, example in enumerate(examples):
        row_splits.append(example.shape[0] + row_splits[i])

    return np.concatenate(examples).astype(np.float32) * 10, row_splits


if __name__ == "__main__":

    ragged_data, row_splits = fake_ragged_data()

    # In my real data zeros exist, so I pad and mask with another value
    mask_value = 99

    x = tf.RaggedTensor.from_row_splits(ragged_data, row_splits).to_tensor(
        default_value=mask_value, shape=(None, 5, 3)
    )
    # print(x[0])

    # create model (no need to compile, I am not training in this example)
    model = tf.keras.Sequential()
    model.add(tf.keras.layers.Masking(mask_value))
    model.add(tf.keras.layers.GRU(6))
    # model.add(tf.keras.layers.LSTM(6))

    model(x)
