"""layers.py

This file contains all subclassed layers.

The main goals of these layers are to allow masking of non-existing timesteps.

"""

import numpy as np
import tensorflow as tf


class MaskedNormalization(tf.keras.layers.Layer):
    """custom masked normalization layer"""

    def __init__(
        self,
        mask_value: int = 999,
        input_dims: tuple = (51, 3),
        name: str = "masked_normalizer",
    ):
        super(MaskedNormalization, self).__init__()

        self.mask_value = mask_value
        self.supports_masking = True
 
        self.normalization = tf.keras.layers.experimental.preprocessing.Normalization(axis=2)


    def adapt(self, inputs):
        """redefined adapt to ignore masked values"""
        if not self.normalization.built:
            raise RuntimeError("layer must be \"built\" prior to calling `.adapt()`, see `model.build_graph()`")

        mask = inputs != self.mask_value

        float_mask = tf.cast(mask, dtype=tf.dtypes.float32)

        mean = tf.reduce_sum(inputs * float_mask, axis=[0, 1]) / tf.reduce_sum(
            float_mask, axis=[0, 1]
        )
        # sample variance (divide by: n-1)
        var = tf.reduce_sum(((inputs - mean) ** 2) * float_mask, axis=[0, 1]) / (
            tf.reduce_sum(float_mask, axis=[0, 1])
            - tf.constant(1, dtype=tf.dtypes.float32, shape=(3,))
        )
        count = tf.experimental.numpy.count_nonzero(
            float_mask
        )

        self.normalization.adapt_mean.assign(mean)
        self.normalization.adapt_variance.assign(var)
        self.normalization.count.assign(count)

        self.normalization.finalize_state()
        self.normalization._is_adapted = True

    def call(self, inputs, mask=None):

        # if mask provided mask output values (multiply by zero)
        if mask is not None:
            reshape_mask = tf.transpose(
                tf.expand_dims(tf.cast(mask, dtype=tf.dtypes.float32), axis=0),
                perm=[1, 2, 0],
            )
            return self.normalization(inputs) * reshape_mask

        return self.normalization(inputs)

    def compute_mask(self, inputs, mask=None):
        """return the input_mask directly"""
        return mask


class MaskPassingBidirectional(tf.keras.layers.Layer):
    def __init__(self, units, input_shape=(51, 3), **kwargs):
        super(MaskPassingBidirectional, self).__init__(**kwargs)

        self.layer = tf.keras.layers.Bidirectional(
            MaskPassingGRU(units), input_shape=(input_shape), name="bidirectional"
        )

    def call(self, inputs):
        return self.layer(inputs)

    def compute_mask(self, inputs, mask=None):
        # return the input_mask directly
        return mask


class MaskPassingGRU(tf.keras.layers.Layer):
    def __init__(
        self,
        units,
        go_backwards: bool = False,
        return_sequences: bool = False,
        return_state: bool = False,
        **kwargs,
    ):
        super(MaskPassingGRU, self).__init__(**kwargs)

        self.units = units
        self.go_backwards = go_backwards
        self.return_sequences = return_sequences
        self.return_state = return_state

        self.layer = tf.keras.layers.GRU(units, return_sequences=False)

    def get_config(self):
        config = super().get_config().copy()
        config.update(
            {
                "units": self.units,
                "go_backwards": self.go_backwards,
                "return_sequences": self.return_sequences,
                "return_state": self.return_state,
            }
        )
        return config

    def call(self, inputs):
        return self.layer(inputs)

    def compute_mask(self, inputs, mask=None):
        # return the input_mask directly
        return mask


class MaskPassingRepeatVector(tf.keras.layers.Layer):
    def __init__(self, time_steps, **kwargs):
        super(MaskPassingRepeatVector, self).__init__(**kwargs)

        self.time_steps = time_steps
        self.repeat = tf.keras.layers.RepeatVector(time_steps)

    def get_config(self):
        config = super().get_config().copy()
        config.update({"time_steps": self.time_steps})
        return config

    def call(self, inputs):
        return self.repeat(inputs)

    def compute_mask(self, inputs, mask=None):
        # return the input_mask directly
        return mask


# TODO: implement these layers
class MaskedFlatten(tf.keras.layers.Layer):
    """layer to flatten mask"""

    def __init__(self, time_steps, **kwargs):
        super(MaskedFlatten, self).__init__(**kwargs)
        raise NotImplementedError


class MaskedReshape(tf.keras.layers.Layer):
    """layer to reshape mask"""

    def __init__(self, time_steps, **kwargs):
        super(MaskedReshape, self).__init__(**kwargs)
        raise NotImplementedError

    def compute_mask():
        pass

# TODO: implement similar layer for Conv1D
# https://stackoverflow.com/questions/64346496/applying-a-mask-to-conv2d-kernel-in-keras
class MaskedConv2D(tf.keras.layers.Layer):
    def __init__(self, *args, **kwargs):
        super(MaskedConv2D, self).__init__()
        self.conv2d = tf.keras.layers.Conv2D(*args, **kwargs)

    def build(self, input_shape):
        self.conv2d.build(input_shape[0])
        self._convolution_op = self.conv2d._convolution_op

    def masked_convolution_op(self, filters, kernel, mask):
        return self._convolution_op(
            filters, tf.math.multiply(kernel, tf.reshape(mask, mask.shape + [1, 1]))
        )

    def call(self, inputs):
        x, mask = inputs
        self.conv2d._convolution_op = functools.partial(
            self.masked_convolution_op, mask=mask
        )
        return self.conv2d.call(x)


#TODO: set-up pytest 
def test_masknorm():

    arr = np.arange(1, 3 + 1).repeat(3).reshape(3, 3)
    arr_0 = np.zeros((3, 3))
    arr_999 = arr_0 + 999

    a_arr = np.tile(np.concatenate((arr, arr_0)), (2, 1, 1)).astype(np.float32)
    a_arr[1,3,:] = 2
    # print(a_arr)

    b_arr = np.tile(np.concatenate((arr, arr_999)), (2, 1, 1)).astype(np.float32)
    b_arr[1,3,:] = 2
    # print(b_arr)

    pad_removed = np.concatenate([a_arr[0, :3, :], a_arr[1, :4, :]])
    expected = (pad_removed - np.mean(pad_removed, axis=0)) / (np.var(pad_removed, axis=0, ddof=1) ** (1/2))
    expected = np.stack([np.pad(expected[:3, :], ((0,3), (0,0))), np.pad(expected[3:, :], ((0,2), (0,0)))])
    print(expected)

    ones = tf.ones((1, 6, 3))

    for arr, mask_value in zip((a_arr, b_arr), (0, 999)):
        print(f'\nmask_val = {mask_value}')
        mask = tf.keras.layers.Masking(mask_value=mask_value)
        masknorm = MaskedNormalization(mask_value=mask_value)
        
        # hack to initialize layers
        mask(ones)
        masknorm(ones)

        # calculate and set layer mean and sample variances
        masknorm.adapt(arr)

        res = masknorm(mask(arr))

        assert np.allclose([2,2,2], masknorm.normalization.weights[0].numpy())
        print('means pass')

        assert np.allclose([2/3,2/3,2/3], masknorm.normalization.weights[1].numpy())
        print('vars pass')

        assert np.allclose(expected, res)
        print('results pass')

if __name__ == "__main__":
    test_masknorm()
