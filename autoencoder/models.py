"""models.py

This file contains sub-classed models allowing to make it simple to call encoding after training 
e.g. `encoding = model.encoder(inputs)`

Models:
TODO: model with Conv1D encoding layer
TODO: model with Conv2D encoding layer -- requires tf.expand_dims(..., axis=-1)
TODO: model with ConvLSTM1D encoding layer -- requires tf.expand_dims(..., axis=-1)

Hyperparameter Optimization:
TODO: dropout and regularization tuning for act2vec models

"""

import tensorflow as tf

from .layers import MaskedNormalization, MaskPassingBidirectional, MaskPassingRepeatVector


class Act2Vec(tf.keras.Model):
    """
    Implementation of activty2vec model as described in the paper<https://arxiv.org/abs/1907.05597>.
    """

    def __init__(
        self, latent_dims=128, input_dim=(None, 51, 3), hp=None
    ):
        super(Act2Vec, self).__init__(name="Act2Vec")

        if (latent_dims % 2) != 0:
            raise ValueError("latent_dims must be even (bidirectional encoding layer)")

        self.layer1 = tf.keras.layers.Bidirectional(
            tf.keras.layers.GRU(int(latent_dims / 2), kernel_regularizer='l1'),
            input_shape=(input_dim[1:]),
            name="layer1",
        )
        self.layer2 = tf.keras.layers.RepeatVector(input_dim[1], name="layer2")
        self.layer3 = tf.keras.layers.GRU(
            latent_dims, return_sequences=True, name="layer3"
        )
        self.layer4 = tf.keras.layers.TimeDistributed(
            tf.keras.layers.Dense(input_dim[2]), name="layer4"
        )

    def call(self, inputs):
        x = self.layer1(inputs)
        x = self.layer2(x)
        x = self.layer3(x)
        return self.layer4(x)

    def encoder(self, inputs):
        return self.layer1(inputs)

    def build_graph(self):
        """use this function to initialize a graph and define the shapes when asking for summary()"""
        x = tf.keras.Input(shape=(51, 3), name="FAKE")
        return tf.keras.Model(inputs=[x], outputs=self.call(x), name=self.name)



class DenseEncoder(tf.keras.Model):
    """Dense encoder - decoder implementation"""

    def __init__(self, latent_dims: int = 42, input_dim=(None, 51, 3), hp=None):
        super(DenseEncoder, self).__init__(name="Dense")

        self.encoding_layers = []
        self.decoding_layers = []

        # a very simple auto encoder
        if hp is None:
            self.input_layer = tf.keras.layers.Flatten(name="flatten")
            self.encoding_layers.append(
                tf.keras.layers.Dense(153, name="encoding_input")
            )
            self.encoding_layers.append(
                tf.keras.layers.Dense(latent_dims, name="encoding")
            )
            self.decoding_layers.append(
                tf.keras.layers.Dense(153, name="decoding_final")
            )
            self.decoding_layers.append(
                tf.keras.layers.Reshape((51, 3), name="reshape")
            )
        else: # a dense focused model with tunable number of hidden layers

            # TODO: layers here are not passing the mask correctly, sub-classing required.
            hp.Choice("input_handling", ["raw"])  # ['raw', 'mask', 'mask_norm' , 'batch_norm', 'layer_norm'])

            if "mask_norm" == hp["input_handling"]:
                raise NotImplementedError
                self.input_layer = MaskedNormalization(mask_value=999)
                self.encoding_layers.append(tf.keras.layers.Flatten(name="flatten"))
            elif "mask" == hp["input_handling"]:
                raise NotImplementedError
                self.input_layer = tf.keras.layers.Masking(mask_value=999, name="mask")
                self.encoding_layers.append(tf.keras.layers.Flatten(name="flatten"))
            else:
                self.input_layer = tf.keras.layers.Flatten(name="flatten")

            # create n encoding layers
            for n in range(hp.Int("encoding_layers_n", min_value=1, max_value=3)):
                # inform the optimizer to set parameters for layer inactive if layer does not exist
                with hp.conditional_scope('encoding_layers_n', list(range(n + 1, 3 + 1))):
                    self.encoding_layers.append(
                        tf.keras.layers.Dense(
                            units=hp.Int(
                                f"encoding_layer_{n}_units",
                                min_value=32,
                                max_value=512,
                                step=32,
                            ),
                            activation=hp.Choice(f'encoding_layer_{n}_activation', values=['linear', 'PReLU', 'LeakyReLU', 'ReLU']),
                            name=f"encoding_layer_{n}",
                        )
                    )
                    self.encoding_layers.append(
                        tf.keras.layers.Dropout(
                            rate=hp.Float(
                                f"encoding_dropout_{n}_rate",
                                min_value=0.0,
                                max_value=0.4,
                                step=0.1,
                            ),
                            name=f"encoding_dropout_{n}",
                        )
                    )

            # encoding dimension
            self.encoding_layers.append(
                tf.keras.layers.Dense(latent_dims, name="encoding")
            )

            # create n decoding layers
            for n in range(hp.Int("decoding_layers_n", min_value=1, max_value=3)):
                # inform the optimizer to set parameters for layer inactive if layer does not exist
                with hp.conditional_scope("decoding_layers_n", list(range(n + 1, 3 + 1))):
                    self.decoding_layers.append(
                        tf.keras.layers.Dense(
                            units=hp.Int(
                                f"decoding_layer_{n}_units",
                                min_value=32,
                                max_value=512,
                                step=32,
                            ),
                            activation=hp.Choice(f'decoding_layer_{n}_activation', values=['linear', 'PReLU', 'LeakyReLU', 'ReLU']),
                            name=f"decoding_layer_{n}",
                        )
                    )
                    self.decoding_layers.append(
                        tf.keras.layers.Dropout(
                            rate=hp.Float(
                                f"decoding_dropout_{n}_rate",
                                min_value=0.0,
                                max_value=0.4,
                                step=0.1,
                            ),
                            name=f"decoding_dropout_{n}",
                        )
                    )

            # final decoding layer (to original shape)
            # TODO: generalize
            self.decoding_layers.append(
                tf.keras.layers.Dense(153, name="decoding_final")
            )
            self.decoding_layers.append(
                tf.keras.layers.Reshape((51, 3), name="reshape")
            )


    def call(self, inputs, training=False):
        """perform encoding and decoding"""
        x = self.input_layer(inputs)

        for layer in self.encoding_layers:
            if training and ('dropout' in layer.name):
                x = layer(x, training=training)
            else:
                x = layer(x)

        for layer in self.decoding_layers:
            if training and ('dropout' in layer.name):
                x = layer(x, training=training)
            else:
                x = layer(x)

        return x


    def encoder(self, inputs):
        """return encoding"""
        x = self.input_layer(inputs)

        for layer in self.encoding_layers:
            x = layer(x)

        return x


    def build_graph(self):
        """use this function to initialize a graph and define the shapes when asking for summary()"""
        x = tf.keras.Input(shape=(51, 3), name="FAKE")
        return tf.keras.Model(inputs=[x], outputs=self.call(x), name=self.name)


# TODO: This model is unverified
class maskedAct2Vec(tf.keras.Model):
    """
    Implementation of activty2vec model as described in the paper<https://arxiv.org/abs/1907.05597>.

    with added masking and normalization layers. use 999 pading (or another number not in the data.)
    """

    def __init__(self, latent_dims=8, input_dim=(None, 51, 3), mask_value: int = 999, hp=None):
        super(maskedAct2Vec, self).__init__(name="maskedAct2Vec")

        if (latent_dims % 2) != 0:
            raise ValueError("latent_dims must be even! (bidirectional encoding layer)")

        self.mask = tf.keras.layers.Masking(mask_value=mask_value)
        self.normalization = MaskedNormalization()
        self.layer1 = MaskPassingBidirectional(
            int(latent_dims / 2), input_shape=(input_dim[1:]), name="layer1"
        )
        self.layer2 = MaskPassingRepeatVector(input_dim[1], name="layer2")
        self.layer3 = tf.keras.layers.GRU(
            latent_dims, return_sequences=True, name="layer3"
        )
        self.layer4 = tf.keras.layers.TimeDistributed(
            tf.keras.layers.Dense(input_dim[2]), name="layer4"
        )


    def build_graph(self):
        """use this function to initialize a graph and define the shapes when asking for summary(), ie. `.model_summary()`"""
        x = tf.keras.Input(shape=(51, 3), name="FAKE")
        return tf.keras.Model(inputs=[x], outputs=self.call(x), name=self.name)

    def call(self, inputs):
        x = self.mask(inputs)
        x = self.normalization(x)
        x = self.layer1(x)
        x = self.layer2(x)
        x = self.layer3(x)
        return self.layer4(x)

    def encoder(self, inputs):
        x = self.mask(inputs)
        x = self.normalization(x)
        return self.layer1(x)


# TODO: This model is unverified
class normedAct2Vec(tf.keras.Model):
    """
    Implementation of activty2vec model as described in the paper<https://arxiv.org/abs/1907.05597>.

    Added normalization layer. Use zero padding.
    """

    def __init__(
        self, latent_dims=128, input_dim=(None, 51, 3), pad_value=0, hp=None
    ):
        super(normedAct2Vec, self).__init__(name="normedAct2Vec")

        if (latent_dims % 2) != 0:
            raise ValueError("latent_dims must be even (bidirectional encoding layer)")

        self.normalization = tf.keras.layers.experimental.preprocessing.Normalization(axis=2)

        self.layer1 = tf.keras.layers.Bidirectional(
            tf.keras.layers.GRU(int(latent_dims / 2)),
            input_shape=(input_dim[1:]),
            name="layer1",
        )
        self.layer2 = tf.keras.layers.RepeatVector(input_dim[1], name="layer2")
        self.layer3 = tf.keras.layers.GRU(
            latent_dims, return_sequences=True, name="layer3"
        )
        self.layer4 = tf.keras.layers.TimeDistributed(
            tf.keras.layers.Dense(input_dim[2]), name="layer4"
        )

    def build_graph(self):
        """use this function to initialize a graph and define the shapes when asking for summary(), ie. `.model_summary()`"""
        x = tf.keras.Input(shape=(51, 3), name="FAKE")
        return tf.keras.Model(inputs=[x], outputs=self.call(x), name=self.name)

    def call(self, inputs):
        x = self.normalization(inputs)
        x = self.layer1(x)
        x = self.layer2(x)
        x = self.layer3(x)
        return self.layer4(x)

    def encoder(self, inputs):
        x = self.normalization(inputs)
        return self.layer1(x)

