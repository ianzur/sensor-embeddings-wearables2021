from typing import List
import os

# silence INFO messages
os.environ["TF_CPP_MIN_LOG_LEVEL"] = "1"
# os.environ["CUDA_VISIBLE_DEVICES"] = "-1"

from contextlib import redirect_stdout
from pathlib import Path
from ast import literal_eval
import json

import numpy as np
import pandas as pd
import sklearn.metrics

import keras_tuner as kt
import tensorflow as tf
import tensorflow_datasets as tfds

from autoencoder.models import Act2Vec, DenseEncoder, maskedAct2Vec, normedAct2Vec
from utils.tuners import cvTuner

import smartwatch_gestures.smartwatch_gestures

from utils.plot.plot import plot_latent_space
from utils.utils import split_df, split_ds, to_np, tfds2pd, as_arrays
from utils.cross_validation_splits import cross_validation


# TODO: use a cyclical learning rate or decay schedule
def build_dense(hp=None):
    """"""

    hp.Int("batch_size", 32, 128, 32)

    model = DenseEncoder(hp=hp, latent_dims=hp.values.get("latent_dims"))

    opt = hp.Choice("optimizer", ["adam", "rmsprop", "sgd"])
    lr = hp.Choice("learning_rate", values=[1e-2, 1e-3, 1e-4, 1e-5])

    if opt == "adam":
        optimizer = tf.keras.optimizers.Adam(learning_rate=lr)
    elif opt == "rmsprop":
        optimizer = tf.keras.optimizers.RMSprop(learning_rate=lr)
    elif opt == "sgd":
        optimizer = tf.keras.optimizers.SGD(learning_rate=lr)
    else: 
        raise ValueError

    model.compile(optimizer, loss="mse", metrics=["mse"])

    return model


def build_act2vec(hp=None):
    """"""

    hp.Int("batch_size", 32, 128, 32)

    model_style = hp.Choice("input_handling", ["raw"]) # "norm", "masknorm"

    if model_style == "raw":
        model = Act2Vec(hp=hp, latent_dims=hp.values.get("latent_dims"))
    elif model_style == "norm":
        model = normedAct2Vec(hp=hp, latent_dims=hp.values.get("latent_dims"))    
    elif model_style == "masknorm":
        model = maskedAct2Vec(hp=hp, latent_dims=hp.values.get("latent_dims"))
    else:
        raise ValueError

    opt = hp.Choice("optimizer", ["adam", "rmsprop", "sgd"])
    lr = hp.Choice("learning_rate", values=[1e-2, 1e-3, 1e-4, 1e-5])

    if opt == "adam":
        optimizer = tf.keras.optimizers.Adam(learning_rate=lr)
    elif opt == "rmsprop":
        optimizer = tf.keras.optimizers.RMSprop(learning_rate=lr)
    elif opt == "sgd":
        optimizer = tf.keras.optimizers.SGD(learning_rate=lr)
    else: 
        raise ValueError

    model.compile(optimizer, loss="mse", metrics=["mse"])

    return model


def train(hp, build, results_dir, model_name: str):
    """used to train the model after hyperparameter selection
    
    hp: kerastuner -- hyperparameter object
    build: function used to build the model with correct hyperparams
    results_dir: where to save training ckpts
    model_name: str
    
    """
    info = {}

    # pad length
    max_len = 51

    # set mask value
    mask_val = 0
    if "masknorm" in hp["input_handling"]:
        mask_val = 999

    ### DATA PREP ###
    # split data
    # TODO: generalize
    train, _, test = split_df(df, [1, 2, 3, 4, 5, 6], [], [7, 8])

    # separate labels, discard labels(this is an autoencoder)
    x_train, _ = as_arrays(train, with_row_splits=True)
    x_test, _ = as_arrays(test, with_row_splits=True)

    # convert to tensors
    x_train = tf.RaggedTensor.from_row_splits(x_train[0], x_train[1]).to_tensor(
        default_value=mask_val, shape=(None, max_len, 3)
    )
    x_test = tf.RaggedTensor.from_row_splits(x_test[0], x_test[1]).to_tensor(
        default_value=mask_val, shape=(None, max_len, 3)
    )

    # this is an autoencoder, y == x
    y_train = x_train
    y_test = x_test

    model = build(hp)
    # model.build_graph().summary()

    # if input data will be normalize in the first layer, we also want to compare output to normalized input
    # this normalization is a non-trainable layer, values are set by the `.adapt()` call below
    if "norm" in hp.values["input_handling"]:
        model.build_graph() # hack to initialize normalization layer
        model.normalization.adapt(x_train)
        y_train = model.normalization(y_train)
        y_test = model.normalization(y_test)
        info["normalization_means"] = model.normalization.weights[0].numpy().tolist()
        info["normalization_variances"] = (
            model.normalization.weights[1].numpy().tolist()
        )

    hist = model.fit(
        x_train,
        y_train,
        validation_data=(x_test, y_test),
        batch_size=hp.values["batch_size"],
        epochs=10,  # max number ( early stopping expected )
        verbose=1,
        callbacks=[
            ## Early Stop ##
            tf.keras.callbacks.EarlyStopping(
                monitor="val_loss",
                min_delta=0.001,
                patience=20,
                verbose=0,
                mode="auto",
                baseline=None,
                restore_best_weights=True,
            ),
            ## Save checkpoints ##
            tf.keras.callbacks.ModelCheckpoint(
                results_dir + "/ckpt",  # file path/name to save the model
                monitor="val_loss",
                verbose=0,
                save_best_only=True,
                save_weights_only=True,
                mode="auto",
                save_freq="epoch",  # If save_freq is an integer, it will save after n batches (not epochs)
            ),
            ## TODO: add tensorboard
        ],
    )

    info = {**info, **hist.history}

    info["total_epochs"] = len(hist.history["loss"])
    info["best_epoch"] = min(
        range(len(hist.history["val_loss"])), key=hist.history["val_loss"].__getitem__
    )

    test_res = model.evaluate(x_test, y_test, return_dict=True)

    for k, val in test_res.items():
        info["test_" + k] = val

    # get predictions from full model
    y_pred = model.call(x_test).numpy()

    info[
        "explained_variance_sklearn_flattened"
    ] = sklearn.metrics.explained_variance_score(
        y_test.numpy().flatten(),
        y_pred.flatten(),
        multioutput="uniform_average", # default
    )

    info[
        "explained_variance_sklearn_samplewise"
    ] = sklearn.metrics.explained_variance_score(
        y_test.numpy().reshape(y_test.shape[0], 153),
        y_pred.reshape(y_test.shape[0], 153),
        multioutput="uniform_average", # default
    )

    # numpy.var() flattens by default
    info["explained_variance_numpy"] = 1 - (
        np.var(y_test.numpy() - y_pred) / np.var(y_test.numpy())
    )

    # TODO, why are these different?
    # print(info["explained_variance_sklearn_flattened"])
    # print(info["explained_variance_sklearn_samplewise"])

    # return trained model (with best weights)
    return model, info


if __name__ == "__main__":

    # slow and doesn't look like its working at first, but trust
    ds, ds_info = tfds.load(
        "smartwatch_gestures",
        data_dir="./data",
        split="train",  # currently there are no pre-defined train/val/test splits
        with_info=True,
        as_supervised=False,  # set False to return participant & attempt numbers, in addition to defined (features, gesture) tuple
    )

    # convert dataset to pandas Dataframe (this shouldn't be necessary, but to split the data how I want, it is required.)
    df = tfds2pd(ds, ds_info)

    # use this to select model type to build
    # select = "act2vec"

    for select in ["act2vec"]: #, "dense"

        if select == "dense":
            build = build_dense
        # elif select == 'conv':
        #     build = build_conv
        elif select == "act2vec":
            build = build_act2vec
        else:
            raise ValueError

        model_results = {}

        # every 8 latent dims
        for latent_dims in range(8, 160 + 1, 8):
            hp = kt.HyperParameters()

            # fix latent dimension size for this model
            hp.Fixed("latent_dims", latent_dims)

            full_model_name = f"{select}_dims{latent_dims}"
            model_results[full_model_name] = {}
            
            train_results = f"./train_results/{select}/dims{latent_dims}/"

            # The sub-classed tuner object (which I have subclassed for cross-validation) currently will only handle oracle=hyperband().
            # some changes will be required to make sure all the requirements for other "oracles" are met.
            tuner = cvTuner(
                oracle=kt.oracles.HyperbandOracle(
                    "val_loss",  # A string or keras_tuner.Objective instance. If a string, the direction of the optimization (min or max) will be inferred.
                    1,  # max_epochs
                    factor=3,  # reduction factor for the number of epochs and number of models for each bracket
                    hyperband_iterations=1,  # the number of times to iterate over the full Hyperband algorithm, It is recommended to set this to as high a value as is within your resource budget. (default=1)
                    seed=None,
                    hyperparameters=hp,  # used to fix number of latent dimensions for this loop.
                    allow_new_entries=True,
                    tune_new_entries=True,
                ),
                hypermodel=build,
                directory=f"./tnn/{select}",
                project_name=full_model_name.split("_")[1],
                overwrite=True,
            )

            tuner.search(df, cross_validation)

            # query the (untrained) best model found during the search process.
            best_hps = tuner.get_best_hyperparameters()[0]
            model_results[full_model_name]["best_hyperparams"] = tuner.get_best_hyperparameters()[0].values
            # print(tuner.results_summary(num_trials=1))

            # returns trained model
            model, info = train(
                tuner.get_best_hyperparameters()[0], build, train_results, full_model_name
            )
            model_results[full_model_name]["test_results"] = info

            # dump this model info to a file
            with open(Path(train_results) / "info.json", "w") as fh:
                json.dump(info, fh, indent=4)

            # plot model structure to file
            # TODO: more informative plotting method
            tf.keras.utils.plot_model(
                model.build_graph(),
                # layer_range=[r'mask|flatten', r'reshape'], # this also fails for some reason
                to_file=f"{train_results}/{full_model_name}_structure.png",
                dpi=96,
                show_shapes=True,
                show_layer_names=True,  # show shapes and layer name
                expand_nested=True,
                # show_layer_activations=True # TODO: upgrade keras (without breaking this workflow) to add this param
            )

            # print model summary to file
            with open(f"{train_results}/{full_model_name}_summary.txt", "w") as fh:
                with redirect_stdout(fh):
                    model.summary()

            # load data from csv formate depend on model input(mask) 999 padded or 0 padded
            # amke sure everything remains in the same order
            if "masknorm" in best_hps.values["input_handling"]:
                data_csv = "./data/999padded_gesture_data.csv"
            else:
                data_csv = "./data/0padded_gesture_data.csv"

            gesture_df = pd.read_csv(
                data_csv, converters={"example": literal_eval}
            )
            gesture_df["example"] = gesture_df["example"].apply(to_np)
            gesture_tensor = np.stack(gesture_df["example"].to_numpy())

            # get encodings for all 
            encoded = model.encoder(gesture_tensor)

            gesture_df = gesture_df.rename(
                columns={"participant": "user", "gesture": "label"}
            )
            encoded_df = pd.DataFrame(encoded.numpy(), dtype=np.float32)
            # convert to lists and save
            encoded_df["examples"] = encoded_df.values.tolist()

            gesture_df["example"] = encoded_df["examples"].apply(
                lambda x: list(map(np.float32, x))
            )
            gesture_df.to_csv(
                f"{train_results}/{full_model_name}_encoded_gesture_data.csv", index=False
            )
            train_participants = [1,2,3,4,5,6]
            test_participants = [7,8]

            # plot encodings latent space
            # train
            plot_latent_space(
                np.stack(gesture_df[gesture_df['user'].isin(train_participants)]['example']),
                gesture_df[gesture_df['user'].isin(train_participants)]['label'].to_numpy(),
                ds_info.features["gesture"].names,
                title=f"tSNE decomposition for training examples\nmodel={select} dims={latent_dims} input={best_hps.values['input_handling']}",
                to_file=f"{train_results}/{full_model_name}_train.png",
                show=False
                )
            # test
            plot_latent_space(
                np.stack(gesture_df[gesture_df['user'].isin(test_participants)]['example']),
                gesture_df[gesture_df['user'].isin(test_participants)]['label'].to_numpy(),
                ds_info.features["gesture"].names,
                title=f"tSNE decomposition for test examples\nmodel={select} dims={latent_dims} input={best_hps.values['input_handling']}",
                to_file=f"{train_results}/{full_model_name}_test.png",
                show=False
                )

    info_df = pd.concat({k: pd.DataFrame(v).T for k, v in model_results.items()}, axis=0).T.stack().swaplevel().sort_index()
    info_df.to_csv(f"./train_results/{select}/{select}_info_df.csv")
