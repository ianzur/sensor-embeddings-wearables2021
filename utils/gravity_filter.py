""" gravity_filter.py

methods to filter gravity from gesture recordings.

1. high pass filter
   gravity is constant, essentially a 0 Hz signal recorded by the accelerometer.
   applying a high pass filter should remove the gravity component and leaving only gesture data.

TODO: test if preprocessing data with high pass filter aids learning

"""

from typing import Iterable, Sequence

import numpy as np

from plot import plot_comparison


def high_pass_filter(x: Iterable[Sequence[float]]): #, time: Union[float, int, Sequence]) -> Iterable[Sequence[float]]:
    """remove gravity with high pass filter
    
    Notes:

        tau = t = RC
        cut-off freq f_c = \frac{1}{2*\pi*t}
        \therfore t = RC = \frac{1}{2\pif_c} 

        dT ~= 0.1 seconds (10 samples per second)

        alpha = 0.94; t = 0.1; f_c = 1.59 Hz
        alpha = 0.76; t = 0.5; f_c = 0.32 Hz

        ..https://en.wikipedia.org/wiki/High-pass_filter#Algorithmic_implementation
    """

    # alpha is calculated as t / (t + dT)
    # with t, the low-pass filter's time-constant
    # and dT, the event delivery rate

    alpha = 0.85

    y = np.zeros(x.shape)
    y[0] = x[0]

    # y[i] := α × (y[i-1] + x[i] - x[i-1])
    for i in range(2, x.shape[0]):
        y[i, 0] = alpha * (y[i-1, 0] + x[i, 0] - x[i-1, 0])
        y[i, 1] = alpha * (y[i-1, 1] + x[i, 1] - x[i-1, 1])
        y[i, 2] = alpha * (y[i-1, 2] + x[i, 2] - x[i-1, 2])

    return y


def test_high_pass_filter():

    # create a sin wave at 1 - 60Hz?
    # sample rate of ~ 10 Hz; Nyquist freq = 0.5 * freq_{sampling rate} ~= 5 Hz

    freq = 233 # Hz
    duration = 5 # seconds (~ longest sample)

    # 10 Hz sampling rate
    t = np.linspace(0, duration, num=(duration * 10))
    y = 3 * np.sin(2 * np.pi * freq * t)

    # is original sine wave returned?
    filtered = high_pass_filter(np.array([y+2, y+3, y+1]).T)

    plot_comparison(np.tile(y, (3,1)).T, filtered, t, title='full "gesture"')

    plot_comparison(np.tile(y, (3,1)).T[7:,:], filtered[7:,:], t[7:], title='trimmed off first 7 samples')

    # print(np.tile(y, (3,1)).T - filtered)

    # fails this assertion... (first few samples are way off..)
    # assert np.allclose(filtered, np.tile(y, (3,1)).T, rtol=0.1, atol=1e-3)

    assert np.allclose(filtered[7:,:], np.tile(y, (3,1)).T[7:,:], rtol=.5, atol=0.75)


if __name__=="__main__":
    test_high_pass_filter()
