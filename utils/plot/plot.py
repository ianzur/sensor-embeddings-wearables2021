# ----------------------------------------------------------------------------
# "THE BEER-WARE LICENSE" (Revision 42):
# <ian.zurutuza@gmail.com> wrote this file.  As long as you retain this notice 
# you can do whatever you want with this stuff. If we meet some day, and you 
# think this stuff is worth it, you can buy me a beer in return.  Ian Zurutuza
# ----------------------------------------------------------------------------

from typing import List, Tuple, Union

from sklearn import manifold
import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf


class marker_generator:
  """yields unique marker color combos, probably the most complicated way to do this, lol"""
  def __init__(
      self,
      colors: List[str] = ['r', 'c', 'k', 'y', 'm', 'g'],
      markers: List[str] = ['+', '^', 'o', 'd', '*', 'p', 's'],
      as_tuple: bool = False,
      repeat: bool = False
    ) -> None:

    if repeat == True:
      raise NotImplementedError()
  
    self.colors = colors
    self.color_i = -1
    self.color_max = len(colors)

    self.markers = markers
    self.marker_i = -1
    self.marker_max = len(markers)

    self.requires_rotate = False

    more_markers = True if self.marker_max > self.color_max else False
    mod = self.marker_max % self.color_max if more_markers else self.color_max % self.marker_max

    if (self.marker_max == self.color_max) or (mod == 0):
      self.requires_rotate = True

    self.as_tuple = as_tuple
    self.repeat = repeat

    self.max = self.color_max * self.marker_max
    self.returned = -1

  def __iter__(self):
    return self

  def __next__(self):
    self.returned += 1

    if self.returned < self.max:
      self.marker_i += 1
      self.color_i += 1

      if self.color_i == self.color_max:          
        self.color_i = 0
      if self.marker_i == self.marker_max:
        if self.requires_rotate: # rotate list
          self.markers = (self.markers[1:] + self.markers[:1])
        self.marker_i = 0

      if self.as_tuple:
        return self.colors[self.color_i], self.markers[self.marker_i]
      
      return "".join((self.colors[self.color_i], self.markers[self.marker_i]))

    raise StopIteration('ran out of unique marker options')


def plot_latent_space(X, y, classes: List[str], title: str, to_file, random_seed: int=None, show: bool=False):
  """
  This function employs tSNE to convert the latent space to plottable 2D representation.

  figure shown and saved to: `to_file` (format is decided based on extension)

  Args:
    X: arraylike 2D (<samples>, <features>)
    y: arraylike 1D (samples,)
    classes: list of strings, order must match 0-indexed class integers
    title: str, "_" chars are removed from string when titling plot ("_" are left in filename during save)
    random_seed: default=None, set seed for more reproducible results
  
  """
  # X = tf.cast(X, dtype='float32')
  tsne = manifold.TSNE(n_components=2, init='random', learning_rate="auto", random_state=random_seed, perplexity=30.0)
  X_embedded = tsne.fit_transform(X)

  plts = ((i, s) for i, s in zip(range(len(classes)), classes))

  marker = marker_generator(as_tuple=True)
  fig = plt.figure(figsize=(8,6))

  # for each class plot tSNE scatter
  for i, l in plts:
      c, m = next(marker)
      plt.scatter(
        X_embedded[y == i][:, 0],
        X_embedded[y == i][:, 1],
        marker = m,
        color = c,
        alpha = 0.3,
        label = l
      )

  plt.legend(loc='lower left', ncol=5, fontsize=8)
  plt.suptitle(title.replace('_', ' '))
  plt.tight_layout()
  plt.savefig(to_file, dpi=300, facecolor='white')
  if show:
    plt.show()
  else:
    plt.close(fig)


def plot_comparison(x, y, t=None, mask_value: int=None, mask=None, title: str="", show: bool=False):
  """
  param
  -----
  x: array like (N,3)

  y: array_like (N,3)

  """

  if x.shape != y.shape:
    raise ValueError(f'Shape\'s do not match x({x.shape}) != y({y.shape})')

  if mask_value is not None:
    mask = (x != mask_value)[:,0]
    x = x[mask, :]
    y = y[mask, :]

  if mask is not None:
    x = x[mask, :]
    y = y[mask, :]

  xlabel = 'time (ns)'

  if t is None:
    xlabel = 'time step'
    t = np.arange(x.shape[0])

  axs_name = ['x', 'y', 'z']

  fig, axs = plt.subplots(3,1, sharex='all')

  for i in range(3):
    axs[i].plot(t, x[:,i], label='original')
    axs[i].plot(t, y[:,i], label='reconstructed')
    axs[i].set_ylabel(axs_name[i], rotation=0)

  lines, labels = fig.axes[-1].get_legend_handles_labels()
  fig.legend(lines, labels, loc='upper right')

  plt.xlabel(xlabel)
  plt.suptitle(title)
  plt.show()
