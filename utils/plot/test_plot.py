"""test_plot.py

verify marker generator return unique markers

"""

import pytest 

from plot import marker_generator

test_data = [
  (['a','b','c'], list(str(i) for i in range(8))),
  (['a','b','c','d','e'], list(str(i) for i in range(3))),
  (['a','b','c','d'], list(str(i) for i in range(4)))
]

@pytest.mark.parametrize(
  "colors,markers", test_data, ids=['more markers', 'more colors', 'equal markers and colors']
)
def test_marker_gen(colors, markers):
  """marker gen should only return unique color marker combinations
  
  i.e. len(returned) == len(set(returned))
  """

  # more markers
  test = 'more markers'
  marker_gen = marker_generator(colors=colors, markers=markers)
  l = []
  try:
    while True:
      l.append(next(marker_gen))
  except StopIteration:
    pass

  s = set(l)
  assert len(l) == len(s)

if __name__=='__main__':
  test_marker_gen()