# --------------------------------------------------------------------
# "THE BEER-WARE LICENSE" (Revision 42):
# <ian.zurutuza@gmail.com> wrote this file. You can do whatever you 
# want with this stuff. If we meet some day, and you think this stuff
# is worth it, you can buy me a beer in return.         Ian Zurutuza
# --------------------------------------------------------------------
"""plot dataset splits"""

import matplotlib.pyplot as plt
from matplotlib.patches import Patch
import numpy as np
import pandas as pd
import tensorflow as tf
import tensorflow_datasets as tfds

from ..utils import tfds2pd
from ..cross_validation_splits import cross_validation


def participant_int_axis(x):
  """custom function used to create secondary axis -- TODO: generalize for any dataset"""
  return ((x - 406.375 / 2 ) / 406.375) + 1

def inv_participant_int_axis(x):
  """custom function used to create secondary axis -- TODO: generalize for any dataset"""
  return ((x + - 406.375 / 2 ) * 406.375 ) - 1


def create_plot(splits, participant_counts, title: str):
  """create stacked bar chart to demonstrate data splits
  
  Args:
    splits:
      list of dicts eg. {'train': [1,2], 'val': [...], 'test': [...]}
    participant_counts:
      pd.Series, index = participant number, containing count of examples from that participant
    title:
      string used to title plot (and to save figure spaces are replaced with "_")
  
  """

  train_color = '#4daf4a'
  val_color = '#dede00'
  test_color = '#e41a1c'

  colors = []

  for i in range(len(splits)):
    tmp_color = []
    for j in participant_counts.index:
      if j in splits[i]['train']:
        tmp_color.append(train_color)
      elif j in splits[i]['val']:
        tmp_color.append(val_color)
      else:
        tmp_color.append(test_color)
    colors.append(tmp_color)

  colors = np.array(colors)
  # print(colors)
     
  participant_counts_df = pd.concat([participant_counts] * len(splits), axis=1)
  participant_counts_df.columns = list(range(len(splits)))
  participant_counts_df = participant_counts_df.transpose()
  participant_counts_df.index.name = 'fold'
  # print(participant_counts_df)

  arr = participant_counts_df.to_numpy()
  fig, ax = plt.subplots(figsize=(10,4))
  
  y_pos = np.linspace(.2*len(splits), 0, num=len(splits))
  # print(y_pos)

  for i, _ in enumerate(splits):
    pos = [0] * len(splits)
    for j in participant_counts.index:
      ax.barh(y_pos, arr[:, j-1], left=pos, color=colors[:, j-1], height=0.25, edgecolor='white', align='center', alpha=1)
      pos += arr[:, j-1]

  legend_elements = [
    Patch(facecolor=train_color, label='Train'),
    Patch(facecolor=val_color, label='Validation'),
    Patch(facecolor=test_color, label='Held Out Test')
    ]
  ax.legend(handles=legend_elements, framealpha=1)

  ax.set_xlabel('examples')
  ax.set_yticks(y_pos)
  ax.set_yticklabels([f'fold {i+1}' for i in range(len(splits))])

  # remove spines
  ax.spines['top'].set_visible(False)
  ax.spines['right'].set_visible(False)
  ax.spines['left'].set_visible(False)

  # secondary axis
  secax = ax.secondary_xaxis('top', functions=(participant_int_axis, inv_participant_int_axis))
  secax.set_xlabel('participant')

  plt.ylim(-0.2, .3 * len(splits) - .1)
  plt.xlim(0, sum(arr[0]))

  plt.suptitle(title)

  plt.tight_layout()

  plt.savefig(title.replace(' ', '_')+'.png', dpi=600)
  plt.show()

if __name__ == "__main__":

  ds, ds_info = tfds.load(
    'smartwatch_gestures',
    data_dir='./data',
    split='train', 
    with_info=True,
    as_supervised=False, # set False to return participant & attempt numbers, in addition to defined (features, gesture) tuple
  )

  df = tfds2pd(ds, ds_info)

  del ds
  del ds_info

  g = df.groupby(by=['participant', 'gesture'])
  each_participant_gesture_attempt_count = g['attempt'].agg('nunique')
  each_participant_attempt_count = each_participant_gesture_attempt_count.groupby('participant').sum()

  create_plot(cross_validation, each_participant_attempt_count, 'auto-encoder dataset splits')


