"""tuner.py

This file contains the subclassed tuner object we used to perform hyperparameter optimization

TODO: Build separate tuner object to allow Bayesian Optimization "Oracle"
TODO: filter tf.data.DataSet object instead of converting to and passing pandas dataframe

"""
from typing import List

import keras_tuner as kt
import numpy as np
import tensorflow as tf

from .utils import as_arrays, split_df, split_ds


class cvTuner(kt.engine.tuner.Tuner):
    """subclassed tuner object to perform cross validation during training"""

    def run_trial(self, trial, df, splits, max_len: int = 51, *fit_args, **fit_kwargs):

        val_losses = []
        hp = trial.hyperparameters

        # zero pad is default
        mask_val = 0
        if "masknorm" in hp["input_handling"]:
            mask_val = 999

        for i, fold in enumerate(splits):
            # print(f'fold_{i}:\t train: {fold["train"]}\t val: {fold["val"]}')

            ### DATA PREP ###
            # TODO: use tf.data.Dataset api -- something like this:
            # train, val, _ = split_ds(ds, fold["train"], fold["val"], fold["test"])
            # x_train = train.map(lambda x: x["accelerometer"])
            # x_train = x_train.padded_batch(hp.values["batch_size"], (51,3), padding_values=tf.constant(mask_val, tf.dtypes.float32))

            # TODO: preprocessing model / .map(preprocessing) (normalize, mean_train / std_train)


            # split data (using pd.dataframe)
            train, val, _ = split_df(df, fold["train"], fold["val"], fold["test"])

            # separate labels, discard labels(this is an autoencoder)
            x_train, _ = as_arrays(train, with_row_splits=True)
            x_val, _ = as_arrays(val, with_row_splits=True)

            # convert to tensors
            x_train = tf.RaggedTensor.from_row_splits(x_train[0], x_train[1]).to_tensor(
                default_value=mask_val, shape=(None, max_len, 3)
            )
            x_val = tf.RaggedTensor.from_row_splits(x_val[0], x_val[1]).to_tensor(
                default_value=mask_val, shape=(None, max_len, 3)
            )

            # this is an autoencoder, y == x
            y_train = x_train
            y_val = x_val

            ### BUILD MODEL ###
            # if this is an extension of a previously explored model, load it
            if "tuner/trial_id" in hp:
                past_trial = self.oracle.get_trial(hp["tuner/trial_id"])
                model = self.load_model(past_trial)
            else:
                model = self.hypermodel.build(hp)

            epochs_to_train = (
                hp.values["tuner/epochs"] - hp.values["tuner/initial_epoch"]
            )

            ### HANDLING
            # if input data will be normalize in the first layer, we also want to compare output to normalized input
            # this normalization is a non-trainable layer, values are set by the `.adapt()` call below
            if "norm" in hp.values["input_handling"]:
                model.build_graph() # hack to initialize normalization layer
                model.normalization.adapt(x_train)
                y_train = model.normalization(y_train)
                y_val = model.normalization(y_val)

            model.fit(
                x_train,
                y_train,
                batch_size=hp.values["batch_size"],
                epochs=epochs_to_train,
                verbose=0,
            )
            val_losses.append(model.evaluate(x_val, y_val, verbose=0)[0])

        # average validation losses
        self.oracle.update_trial(trial.trial_id, {"val_loss": np.mean(val_losses)})
        self.save_model(trial.trial_id, model)
