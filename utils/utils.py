"""utils.py

This file contains general utility functions

Methods:
    split_df(dataframe, [int], [int], [int]) -> returns three dataframes 
        split dataframe into train, validation, test sets         

    split_ds()
        split tf.data.Dataset object into train, val, test sets

    row_splits()
        return lengths of each sample, use with `tf.RaggedTensor.from_row_splits(2d_array, list_like)`

    as_arrays()
        return dataframe as numpy array with only the data required for training

    read_class_labels()

    to_np()

    feature_wise_mean_and_std()
        calc column wise means and stds

    preprocess()
        apply normalization

    print_layer_mask_attributes()
        investigate layer masking

    from_which_file()
        which file did this data come from?

"""
from pathlib import Path
from typing import List

import numpy as np
import pandas as pd

# required for type hints
import tensorflow as tf
import tensorflow_datasets as tfds


def split_df(
    df: pd.DataFrame,
    train: List[int],
    val: List[int],
    test: List[int],
    by: str = "participant",
):
    """return dataframes split by column 'by'"""

    df_train = df[df[by].isin(train)]
    df_val = df[df[by].isin(val)]
    df_test = df[df[by].isin(test)]

    return df_train, df_val, df_test


# TODO: untested
def split_ds(
    ds: tf.data.Dataset,
    train: List[int],
    val: List[int],
    test: List[int],
    by: str = "participant",
):
    """return tf.data.Datasets split by attribute"""

    ds_train = ds.filter(lambda x: tf.math.reduce_any(x[by] == train))
    ds_val = ds.filter(lambda x: tf.math.reduce_any(x[by] == val))
    ds_test = ds.filter(lambda x: tf.math.reduce_any(x[by] == test))

    return ds_train, ds_val, ds_test


def tfds2pd(ds: tf.data.Dataset, ds_info: tfds.core.DatasetInfo) -> pd.DataFrame:
    """change tf-dataset into pd.Dataframe"""

    # this returns each gesture as a row (accelerometer & time data columns contain lists)
    df = tfds.as_dataframe(ds, ds_info=ds_info)

    # so, explode
    df = df.explode(
        [f"time_stamps/{f}" for f in ds_info.features["time_stamps"].keys()]
        + ["accelerometer"]
    ).reset_index(drop=True)

    # and rename some columns
    df.columns = df.columns.str.replace("time_stamps/", "time_")

    # expand accelerometer data into it's own columns
    df = pd.concat(
        [
            pd.DataFrame(
                df["accelerometer"].to_numpy().tolist(),
                columns=["_".join(("accel", s)) for s in ["x", "y", "z"]],
            ),
            df.loc[:, df.columns != "accelerometer"],
        ],
        axis=1,
    )

    return df


def row_splits(groups: pd.DataFrame.groupby) -> np.array:
    """use to generate ragged tensors
    this is also probably possible in the smartwatch_gestures.py config, I just don't know what I am doing

    returns:
        np.array, containing the index that starts each recording attempt, and the index of the last row
    """

    splits = np.concatenate(([0], groups.size().to_numpy()))
    splits = np.cumsum(splits)

    return splits.astype(np.int32)


def as_arrays(df: pd.DataFrame, with_row_splits: bool = True):
    """return tuple of x, y for supervised learning

    Args:
        row_splits: bool (default=True)
            returns "x" as tuple (2D array, row_split_array).
            When False, returns "x" examples as 3D array

    returns:
        tuple: (x, y)
            x = examples (accelerometer data)
            y = gesture label

    """

    grouped = df.groupby(["participant", "gesture", "attempt"], sort=False)
    y = grouped["gesture"].first().to_numpy()

    if with_row_splits:
        x = [
            df[["accel_x", "accel_y", "accel_z"]].to_numpy(dtype=np.float32),
            row_splits(grouped),
        ]
        return x, y
    else:
        x = []
        for k, g in grouped:
            x.append(g[["accel_x", "accel_y", "accel_z"]].to_numpy(dtype=np.float32))
        x = np.array(x, dtype=object)
        return x, y


def read_class_labels(path: Path):
    """return list of class labels, see: ./smartwatch_gestures/class_labels.txt"""

    with open(path, "r") as fh:
        class_names = fh.read().splitlines()

    return class_names


def to_np(arr):
    """transform array of lists to array of arrays

    use `np.stack()` to transform to actual 2d array
    """

    return np.array(arr)


def feature_wise_mean_and_std(array):
    """return std and mean column wise"""

    if len(array.shape) != 2:
        raise RuntimeError(
            "this function expects a 2d array, 3d -> 2d use `np.stack()`"
        )

    means = np.array(np.mean(array, axis=0))
    stds = np.array(np.std(array, axis=0))

    return means, stds


def preprocess(array, means, stds):
    """apply mean and std"""
    return (array - means) / stds


def print_layer_mask_attributes(model):
    for i, l in enumerate(model.layers):
        print(f'layer {i}: {l}')
        print(f'has input mask: {l.input_mask}')
        print(f'has output mask: {l.output_mask}')


def from_which_file(participant: int, gesture: int, attempt: int) -> str:
    """in which file did this data originate? I created to double check thing were working as expected"""

    participant = f'U{participant:02}' # add the U back
    gesture = f'{gesture+1:02}' # tensorflow indexes class label from 0, gestures-data indexes from 1
    attempt = f'{attempt:02}.txt' # add file extension

    return '/'.join([participant, gesture, attempt])