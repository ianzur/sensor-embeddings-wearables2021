"""
defined cross validation splits by participant
"""

# defined nested cross validation loops (by participant)
# not used
nested_cross_validation = [
    [
        {"train": [1, 2, 3, 4], "val": [5, 6], "test": [7, 8]},
        {"train": [1, 2, 5, 6], "val": [3, 4], "test": [7, 8]},
        {"train": [3, 4, 5, 6], "val": [1, 2], "test": [7, 8]},
    ],
    [
        {"train": [1, 2, 3, 4], "val": [7, 8], "test": [5, 6]},
        {"train": [1, 2, 7, 8], "val": [3, 4], "test": [5, 6]},
        {"train": [3, 4, 7, 8], "val": [1, 2], "test": [5, 6]},
    ],
    [
        {"train": [1, 2, 5, 6], "val": [7, 8], "test": [3, 4]},
        {"train": [1, 2, 7, 8], "val": [5, 6], "test": [3, 4]},
        {"train": [5, 6, 7, 8], "val": [1, 2], "test": [3, 4]},
    ],
    [
        {"train": [3, 4, 5, 6], "val": [7, 8], "test": [1, 2]},
        {"train": [3, 4, 7, 8], "val": [5, 6], "test": [1, 2]},
        {"train": [5, 6, 7, 8], "val": [3, 4], "test": [1, 2]},
    ],
]

# defined cross validation loop (by participant)
# using this
cross_validation = nested_cross_validation[0]