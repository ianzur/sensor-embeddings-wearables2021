# Quick start
### install & config git
```bash
sudo apt install git
git config --global user.name "Fname Lname"
git config --global user.email "your_email_address@example.com"

# check config
git config --global --list
```

### ssh keys 
[how to](https://docs.gitlab.com/ee/ssh/index.html) so you don't have to supply your username & password every time you access the repo. 
> well, you should still have a password on your locking private key.

## fork the repo (recommended)
allows you to freely make changes & experiment *without* affecting the original codebase.
[workflow](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html)

#### local set-up
first fork the repository on gitlab then...
```bash
git clone git@gitlab.com:your-username/sensor-embeddings-wearables2021.git sensor-embeddings-wearables2021-your-username
cd sensor-embeddings-wearables2021-your-username
git remote add upstream git@gitlab.com:ianzur/sensor-embeddings-wearables2021.git
```

#### branching (and staying up to date)
create new branch
```
git checkout -b shiny-new-feature
```

keeping your local master up to date
```
git checkout master
git pull upstream master --ff-only
```

commiting branches
```
# check for files containing local changes (double check you are on the correct branch)
git status 

# add files for commit
git add path/to/file-to-be-added.py

# commit changes to current branch
git commit -m "a short description of the changes you made"
git push origin shiny-new-feature
```

your changes are now online, in *your* forked copy of the repository.
submit a merge/pull request (via browser)

## or directly clone repo (must be a collaborator) 
you now have the power to directly change the code base. If you do this **please properly use branches**, so we can easily track changes. see "commiting branches" above.
```bash
git clone git@gitlab.com:ianzur/sensor-embeddings-wearables2021.git 
cd sensor-embeddings-wearables2021
```
