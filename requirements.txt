tensorflow_datasets==4.4.0
tensorflow==2.6.0
pandas==1.3.2
jupyter==1.0.0
matplotlib==3.4.3
numpy==1.19.5
